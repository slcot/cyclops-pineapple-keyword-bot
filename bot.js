(function(botHandler){

	var BOT_NAME = "Pineapple Keyword Bot";
	var msgbox = null;

	var exports = {
		onStart: function onStart(){
			(new dialogBox({
				type: dialogBox.OKAY,
				instructions: BOT_NAME + ' is now listening.'
			})).open();

			var pineappleImg = '<img src="/js/bot-api/bots/pineapple.png"/>';

			msgbox = new dialogBox({
				type: dialogBox.OKAY,
				instructions: 'Did somebody say pineapple?' + pineappleImg,
				okayText: 'Thank you for acknowledging.'
			});
		},

		onTranscription: function onTranscription(result){
			if(!result.isFinal && 
				result.transcript.toLowerCase().indexOf('pineapple') != -1
				&& !msgbox.isOpen()){
				msgbox.open();
			}
		}
	};

	botHandler.loadExportsFor(BOT_NAME, exports);

})(window.botHandler);
